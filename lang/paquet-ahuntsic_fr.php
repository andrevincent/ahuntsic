<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Fichier produit par PlugOnet
// Module: paquet-ahuntsic
// Langue: fr
// Date: 18-07-2023 16:27:11
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'ahuntsic_description' => 'Activer ce plugin pour sélectionner le squelette {{Ahuntsic}} pour votre site.',
	'ahuntsic_slogan' => 'Un jeu de squelettes multilingues à caractère générique',
);
?>